# AUTOMATIC FACTORY FARM:
Permet de créer une ferme automatique **(blé, des carottes, des patates, des betteraves ou des verrues du Nether)**

Se craft en 3 étapes.

Vidéo exemple: [Automatic factory farm en action](https://www.youtube.com/watch?v=Sq65VV9MWW4)

### Etape 1: Crafter le component of industrial farm
Items:
- 2 iron_ingot
- 1 water_bucket
- 2 dirt
- 2 iron_hoe
- 1 dropper
- 1 redstone_block

![Component of industrial farm](https://www.zupimages.net/up/18/38/nu6l.png)

### Etape 2: Crafter l'automatic factory farm
Items:
- 4 iron_block
- 1 component of industrial farm
- 24 wheat_seeds
- 1 hopper
- 1 redstone_block

![Automatic Factory Farm](https://www.zupimages.net/up/18/38/v4xv.png)

### Etape 2: Crafter les barrières automatiques
Items:
- 36 fence
- 4 fence_gate
- 12 torch
- 1 automatic factory farm

⚠️ Vous pouvez utiliser n'importe quel type de bois.

![Automatic Factory Farm](https://nsa39.casimages.com/img/2018/07/06/180706040314957615.png)

### Utilisation

Cette dernière peut cultiver du blé, des carottes, des patates, des betteraves ou des verrues du Nether à condition que vous lui fournissiez le nécessaire pour qu'elle puisse débuter:
- Une zone de 9x9 plate où il faudra la poser au milieu. Je vous conseille de placer au centre de l'eau pour hydrater les cultures.
- Des graines, elle en conservera 64 pour replanter à tout moment.
- De la poudres d'os, elle pourra en stocker 80. Optionnelles mais utiles car les plantations pousseront beaucoup plus rapidement. Il faudra poser un conteneur au centre et le remplir de poudres d'os.
- Une houe, car elle vous indiquera le centre de la machine et sa culture actuelle. En mode blé elle plantera uniquement du blé. De même pour le mode carotte, patate, betterave et verrue du Nether. Lancez la houe autant de fois que nécessaire pour naviguer entre ses différents modes. Si vous la cassez ou changez de culture entre-temps, elle vous redonnera l’intégralité des graines et des poudres d'os restantes.

⚠️ Pour les verrues du Nether prévoyez un sol en sable des âmes

Cette machine permettra également la récolte des melons, des citrouilles, des cactus et de la canne à sucre sans avoir à faire de préréglages.

Comme vous pouvez le voir sur la vidéo, la machine téléportera sa récolte sur le centre d'un de ses côtés. Vous pouvez changer ce côté en lui lançant un coffre jusqu'à ce que cela vous convienne. 

Pour récupérer votre machine, cassez le bloc du centre là où vous l'avez précédemment déposé.



