# Quarry

Elle minera tout sauf les ‹blocs spéciaux›. Elle descendra tout en bas avec ses 4 coffres et 2 entonnoirs en remplaçant au passage les liquides par du verre. (pour éviter les inondations) 

- bedrock
- dropper
- barrel
- lodestone
- spawner
- furnace
- infested_chiseled_stone_bricks
- infested_cobblestone
- infested_cracked_stone_bricks
- infested_mossy_stone_bricks
- infested_stone
- infested_stone_bricks
- end_gateway
- end_portal
- end_portal_frame
- command_block
- barrier
- repeating_command_block
- chain_command_block
- structure_void
- structure_block

Se craft en 2 étapes.

## Etape 1 : Crafter le component of quarry
Items:
- 16 redstone
- 2 iron_block
- 2 iron_pickaxe
- 2 hopper
- 4 chest
- 1 dropper

![Component of quarry](https://www.zupimages.net/up/18/38/vgra.png)

## Etape 1 : Crafter le component of quarry
Items:
- 4 iron_block
- 1 redstone_block
- 1 component of quarry
- 1 coal_block
- 1 dropper
- 2 diamond

![Quarry](https://nsa39.casimages.com/img/2018/07/06/180706041005607578.png)

## Utilisation

Votre Quarry aura fini de miner lorsqu'en dessous de ses conteneurs se trouve un ‹blocs spéciaux› ou si vous détruisez manuellement en cassant un de ses conteneurs.

Lorsqu'elle s'est cassé, il faudra la recharger avec ses conteneurs dans l'Advanced Crafting Table pour pouvoir la réutiliser, peu importe la disposition des objets.

## Améliorations

### Silk touch

Permet à de nombreux blocs d'être récupéré tels quels à la place des objets normalement lâchés.
Par exemple prenons les diamants: vous ne récupérerez non pas les diamants mais les blocs ‹minerais de diamants›.

![Silk touch](https://www.zupimages.net/up/19/08/bius.png)

### Smelting

Permet à l'inverse de cuire directement les récoltes de la Quarry.

Blocs compatible :
- roche (stone)
- sable
- minerais de fer
- minerais d'or

![Smelting](https://www.zupimages.net/up/19/08/rpny.png)
